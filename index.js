/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:


	function getFullName(){
		let strFullName = prompt("What is your Full Name?");
		console.log("Hello, " + strFullName);
	}

	function getAge(){
		let intAge = prompt("What is your Age?");
		console.log("You are " + intAge + " years old.");
	}

	function getLoc(){
		let strLoc = prompt("Where do you live?");
		console.log("You live in " + strLoc);
	}

	getFullName();
	getAge();
	getLoc();

	alert("Thank you for your answering questions!");
	
	

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function showBand(){
		function showTopOne(){
			console.log("1. The Beatles");
		}
		function showTopTwo(){
			console.log( "2. Metallica");
		}
		function showTopThree(){
			console.log("3. The Eagles");
		}
		function showTopFour(){
			console.log("4. L'arc~en~Ciel");
		}
		function showTopFive(){
			console.log("5. Eraserheads");
		}

		showTopOne();
		showTopTwo();
		showTopThree();
		showTopFour();
		showTopFive();
	}

	showBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function showTopOneMov(){
		console.log("1. The Godfather");
		console.log("Rotten Tomatoes Rating: 97%")
	}
	function showTopTwoMov(){
		console.log("2. The Godfather, Part II");
		console.log("Rotten Tomatoes Rating: 96%")
	}
	function showTopThreeMov(){
		console.log("3. Shawshank Redemption");
		console.log("Rotten Tomatoes Rating: 91%")
	}
	function showTopFourMov(){
		console.log("4. To Kill a Mockingbird");
		console.log("Rotten Tomatoes Rating: 93%")
	}
	function showTopFiveMov(){
		console.log("5. Psycho");
		console.log("Rotten Tomatoes Rating: 96%")
	}

	showTopOneMov();
	showTopTwoMov();
	showTopThreeMov();
	showTopFourMov();
	showTopFiveMov();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

//printUsers(); cannot call function name inside a function variable
//remove parenthesis beside the variable function [printFriends]
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	
	// change from alert to prompt
	let friend1 = prompt("Enter your first friend's name:"); 

	//corrected the typo on prompt function from prom to prompt
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1);
	console.log(friend2); 
	console.log(friend3); //corrected the variable name from friends to friend3
};

/* cannot use the variables because its inside the function scope
console.log(friend1); 
console.log(friend2);
*/

//must invoke the function to run its programs
printFriends();